const express = require('express');
const router = express.Router();
const TopSellingProducts = require('../data/TopSellingProduct.json')
const MainTopBanner = require('../data/mainTopBanner.json')
const ProductCategory = require('../data/productCategory.json')
const shopByRoom = require('../data/shopRoom.json')
const Blogs = require('../data/Blogs.json')
const BlogsComment = require('../data/BlogComment.json')
const CustomerReview = require('../data/CustomerReview.json')





router.get('/Top-Selling-Products',(req,res)=>{
   try {
       if ( TopSellingProducts && TopSellingProducts.Products) {
           res.status(200).json({TopSellingProducts})
       }else{
           res.status(401).json({err:"No Products found"})
       }
   } catch (err) {
       res.status(500).json({err:"something went wrong"})
   }
})


router.get('/Main-Top-Banner',(req,res)=>{
    try {
        if ( MainTopBanner && MainTopBanner.Products) {
            res.status(200).json({ MainTopBanner})
        }else{
            res.status(401).json({err:"No Products found"})
        }
    } catch (err) {
        res.status(500).json({err:"something went wrong"})
    }
 })


 router.get('/Product-Category',(req,res)=>{
    try {
        if ( ProductCategory && ProductCategory.ProductCategoryList) {
            res.status(200).json({ ProductCategory })
        }else{
            res.status(401).json({err:"No Products found"})
        }
    } catch (err) {
        res.status(500).json({err:"something went wrong"})
    }
 })

 
router.get('/Shop-By-Room',(req,res)=>{
    try {
        if ( shopByRoom && shopByRoom.shopByRoom) {
            res.status(200).json(shopByRoom)
        }else{
            res.status(401).json({err:"No Products found"})
        }
    } catch (err) {
        res.status(500).json({err:"something went wrong"})
    }
})


router.get('/Get-Blogs',(req,res)=>{
    try {
        if ( Blogs && Blogs.Blogs) {
            res.status(200).json( Blogs )
        }else{
            res.status(401).json({err:"No Blogs found"})
        }
    } catch (err) {
        res.status(500).json({err:"something went wrong"})
    }
})


router.get('/Get-Blogs-Comments',(req,res)=>{
    try {
        if ( BlogsComment && BlogsComment.BlogComments) {
            res.status(200).json( BlogsComment )
        }else{
            res.status(401).json({err:"No Blogs Comment found found"})
        }
    } catch (err) {
        res.status(500).json({err:"something went wrong"})
    }
})

router.get('/Customer-Review',(req,res)=>{
    try {
        if ( CustomerReview && CustomerReview.customerReview) {
            res.status(200).json( CustomerReview )
        }else{
            res.status(401).json({err:"No Reviews found found"})
        }
    } catch (err) {
        res.status(500).json({err:"something went wrong"})
    }
})


module.exports = router
